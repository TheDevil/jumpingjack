#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "FacebookManager.hpp"

#define OBJ_SPEED 400.0f
#define BG_SPEED 350.0f
USING_NS_CC;

class HelloWorld : public cocos2d::Layer
{
    bool continueScrolling,processTouch, isPlayerJumping;
    int obstacleLastPosition, score;
    Label *scoreLabel;
    Sprite *player;
    Vector<Sprite *> *bgImages;
    Vector<Sprite *> *obstacleArray;
    Action *runAction;//, *jumpAction;
    FiniteTimeAction *jumpAction;
    Menu *btnMenu;
    FacebookManger fbManger;
    
    void scrollbackground(float time);
    void checkCollisionWithObstacles(float delta);
    void updateScore(float delta);
    void resetObstacles();
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    // a selector callback
    void menuCallback(Ref* pSender);
    bool onTouchBegan(Touch* touch, cocos2d::Event* event);
    void onTouchMoved(Touch* touch, cocos2d::Event* event);
    void onTouchEnded(Touch* touch, cocos2d::Event* event);
    void playerJumpEnd();
    void loginToFB();
    void submitScoreToFB();
    void fetchScoreFromFB();
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
    HelloWorld();
    ~HelloWorld();
};

#endif // __HELLOWORLD_SCENE_H__

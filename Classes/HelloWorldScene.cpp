#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

HelloWorld::HelloWorld()
{
    
}

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("res/SpriteSheet.plist", "res/SpriteSheet.png");
    Size screenSize = Director::getInstance()->getWinSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    
    continueScrolling = false;
    processTouch = false;
    isPlayerJumping = false;
    
    bgImages = new Vector<Sprite*>();
    Sprite *bg1 = Sprite::create("res/full-background.png");
    bg1->setPosition(Point(origin.x + screenSize.width/2.0, origin.y + screenSize.height / 2.0));
    addChild(bg1,1);
    bgImages->pushBack(bg1);
    
    Sprite *bg2 = Sprite::create("res/full-background.png");
    bg2->setPosition(Point(origin.x + screenSize.width * 1.49f, origin.y + screenSize.height / 2.0));
    addChild(bg2,1);
    bgImages->pushBack(bg2);
    
    player = Sprite::createWithSpriteFrameName("Run__000.png");
    player->setPosition(Point(origin.x + screenSize.width * 10.0f / 100, origin.y + screenSize.height * 18.0/100));
    this->addChild(player,2);
    player->setVisible(false);
    Animation *runAnim = Animation::create();
    for (int i = 1; i < 10; i++)
    {
        char str[100] = {0};
        sprintf(str, "Run__00%d.png",i);
        runAnim->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(str));
    }
    runAnim->setDelayPerUnit(0.1f);
    runAction = RepeatForever::create(Animate::create(runAnim));
    //        player->runAction(runAction);
    
    Animation *jumpAnim = Animation::create();
    for (int i = 0; i < 10; i++)
    {
        char str[100] = {0};
        sprintf(str, "Jump__00%d.png",i);
        jumpAnim->addSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName(str));
    }
    
    jumpAnim->setDelayPerUnit(0.12f);
    jumpAction = Animate::create(jumpAnim);
    runAction->retain();
    jumpAction->retain();
    
    obstacleArray = new Vector<Sprite*>();
    obstacleLastPosition = screenSize.width / 2.0f;
    
    for(int i =0; i <10; i++)
    {
        int obstacleType = arc4random()%2 + 1;
        char str[100] = {0};
        sprintf(str, "res/crate_%d.png",obstacleType);
        Sprite *obj = Sprite::create(str);
        addChild(obj,4);
        int randomPosition = arc4random_uniform(20) + 25;
        obj->setPosition(origin.x + obstacleLastPosition + randomPosition * screenSize.width / 100.0f, origin.y + screenSize.height * 16.0 / 100);
        obstacleLastPosition = obj->getPosition().x;
        //                log("position %d", obstacleLastPosition);
        obstacleArray->pushBack(obj);
    }
    score = 0;
    char scoreStr[100] = {0};
    sprintf(scoreStr, "%d",score);
    
    scoreLabel = Label::createWithSystemFont(scoreStr, "ComicSansMS", 80);
    scoreLabel->setPosition(Vec2(screenSize.width * 85.0f / 100, screenSize.height * 85.0f / 100));
    scoreLabel->setColor(Color3B::BLUE);
    this->addChild(scoreLabel,2);
    
    auto btn = MenuItemImage::create(
                                     "res/play.png",
                                     "res/play.png",
                                     CC_CALLBACK_1(HelloWorld::menuCallback, this));
    
    btn->setPosition(Vec2(screenSize.width / 2.0, screenSize.height / 2.0));
    
    // create menu, it's an autorelease object
    btnMenu = Menu::create(btn, NULL);
    btnMenu->setPosition(Vec2::ZERO);
    this->addChild(btnMenu, 2);
    
    auto touchListner = EventListenerTouchOneByOne::create();
    touchListner->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
    touchListner->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
    touchListner->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListner, this);
    
    this->schedule(SEL_SCHEDULE(&HelloWorld::scrollbackground),1/60.0f);
    this->schedule(SEL_SCHEDULE(&HelloWorld::checkCollisionWithObstacles),1/60.0f);
    this->schedule(SEL_SCHEDULE(&HelloWorld::updateScore),1/5.0f);
    return true;
}


void HelloWorld::menuCallback(Ref* pSender)
{
            processTouch = !processTouch;
            btnMenu->setVisible(false);
            continueScrolling = true;
            player->setVisible(true);
            player->runAction(runAction);
}

void HelloWorld::resetObstacles()
{
    Size screenSize = Director::getInstance()->getWinSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    obstacleLastPosition = screenSize.width / 2.0f;
    Sprite *obj;
    for (int i = 0; i < obstacleArray->size(); i++)
    {
        obj = obstacleArray->at(i);
        int randomPosition = arc4random_uniform(20) + 25;
        obj->setPosition(origin.x + obstacleLastPosition + randomPosition * screenSize.width / 100.0f, origin.y + screenSize.height * 16.0 / 100);
        obstacleLastPosition = obj->getPosition().x;
    }
    obstacleLastPosition = obj->getPosition().x;
}

void HelloWorld::scrollbackground(float delta)
{
    if (continueScrolling)
    {
        Size screenSize = Director::getInstance()->getWinSize();
        Point origin = Director::getInstance()->getVisibleOrigin();
        for (int i = 0; i < bgImages->size(); i++)
        {
            Sprite *bg = (Sprite*)bgImages->at(i);
            float x;
            float y = bg->getPosition().y;
            if (bg->getPosition().x <= -screenSize.width * 0.49f)
                x = (screenSize.width*1.49f) - BG_SPEED * delta;
            else
                x = bg->getPosition().x - BG_SPEED * delta;
            bg->setPosition(Point(origin.x + x, y));
        }
        for (int i = 0; i < obstacleArray->size(); i++)
        {
            Sprite *obj = obstacleArray->at(i);
            
            float x = obj->getPosition().x;
            float y = obj->getPosition().y;
            obj->setPosition(Point(x - OBJ_SPEED * delta, y));
            if (x < -obj->getContentSize().width)
            {
                int randomPosition = arc4random_uniform(20) + 20;
                obj->setPosition(origin.x + obstacleLastPosition +randomPosition * screenSize.width / 100, obj->getPosition().y);
                obstacleLastPosition = obj->getPosition().x;
            }
        }
        
    }
}

void HelloWorld::checkCollisionWithObstacles(float delta)
{
    Size screenSize = Director::getInstance()->getWinSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    for (int i = 0; i < obstacleArray->size(); i++)
    {
        Sprite *obj = obstacleArray->at(i);
        Rect collisionRect = Rect(obj->getBoundingBox().origin.x + 20, obj->getBoundingBox().origin.y, obj->getBoundingBox().size.width - 20, obj->getBoundingBox().size.height - 20);
        if(obj->getPosition().x > -obj->getContentSize().width && obj->getPosition().x < screenSize.width)
        {
            if(player->getBoundingBox().intersectsRect(collisionRect))
            {
                continueScrolling = false;
                isPlayerJumping = false;
                player->stopAllActions();
                player->setPosition(Point(origin.x + screenSize.width * 10.0f / 100, origin.y + screenSize.height * 18.0/100));
                player->setVisible(false);
                processTouch = false;
                btnMenu->setVisible(true);
                this->resetObstacles();
                submitScoreToFB();
                score = 0;
                break;
            }
        }
    }
}

void HelloWorld::updateScore(float delta)
{
    if(processTouch)
    {
        score ++;
        char scoreStr[100] = {0};
        sprintf(scoreStr, "%d",score);
        scoreLabel->setString(scoreStr);
    }
}

bool HelloWorld::onTouchBegan(Touch* touch, cocos2d::Event* event)
{
    if(!processTouch || isPlayerJumping)
        return false;
    player->stopAllActions();
    isPlayerJumping = true;
    //        player->runAction(jumpAction);
    player->runAction(Spawn::create(jumpAction,
                                    Sequence::create(JumpBy::create(0.5, Vec2(0,280), 85, 1),JumpBy::create(0.5, Vec2(0,-280), 85, 1),
                                                     CallFunc::create(CC_CALLBACK_0(HelloWorld::playerJumpEnd, this)),NULL), NULL));
    return true;
}

void HelloWorld::onTouchMoved(Touch* touch, cocos2d::Event* event)
{
    
}

void HelloWorld::onTouchEnded(Touch* touch, cocos2d::Event* event)
{
    
}



void HelloWorld::playerJumpEnd()
{
    isPlayerJumping = false;
    player->stopAllActions();
    player->runAction(runAction);
}


HelloWorld::~HelloWorld()
{
    bgImages->clear();
    obstacleArray->clear();
    delete bgImages;
    delete obstacleArray;
    runAction->release();
    jumpAction->release();
//    delete fbManger;
}


void HelloWorld::loginToFB()
{
#ifdef SDKBOX_ENABLED
    if(!sdkbox::PluginFacebook::isLoggedIn())
        sdkbox::PluginFacebook::login();
    else
        sdkbox::PluginFacebook::requestPublishPermissions({sdkbox::FB_PERM_PUBLISH_POST,sdkbox::FB_PERM_READ_USER_FRIENDS});
#endif
}

void HelloWorld::submitScoreToFB()
{
    sdkbox::FBAPIParam params;
    params.insert(std::pair<std::string,std::string>("score",scoreLabel->getString()));
    std::string graphApi = "/" + sdkbox::PluginFacebook::getUserID() + "/scores";
    sdkbox::PluginFacebook::api(graphApi, "POST", params, graphApi);
}

void HelloWorld::fetchScoreFromFB()
{
    sdkbox::FBAPIParam params;
    std::string graphApi = "/708971492610963/scores";
    sdkbox::PluginFacebook::api(graphApi, "GET", params, graphApi);

}

//
//  FacebookManager.cpp
//  JumpingJack
//
//  Created by Nidhi on 1/25/17.
//
//

#include "FacebookManager.hpp"
using namespace sdkbox;

FacebookManger::FacebookManger()
{
    sdkbox::PluginFacebook::setListener(this);

}

FacebookManger::~FacebookManger()
{
    sdkbox::PluginFacebook::setListener(NULL);

}

void FacebookManger::onLogin(bool isLogin, const std::string& msg)
{
    sdkbox::PluginFacebook::requestPublishPermissions({FB_PERM_PUBLISH_POST,FB_PERM_READ_USER_FRIENDS});
}

void FacebookManger::onPermission(bool isLogin, const std::string& msg)
{
    printf("onPermission %d %s",isLogin, msg.c_str());
}

void FacebookManger::onAPI(const std::string& tag, const std::string& jsonData)
{
    CCLOG("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());
}

void FacebookManger::onSharedSuccess(const std::string& message)
{
    
}

void FacebookManger::onSharedFailed(const std::string& message)
{
    
}

void FacebookManger::onSharedCancel()
{
    
}

void FacebookManger::onFetchFriends(bool ok, const std::string& msg)
{
    
}

void FacebookManger::onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends )
{
    
}

void FacebookManger::onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg )
{
    
}

void FacebookManger::onInviteFriendsResult( bool result, const std::string& msg )
{
    
}

void FacebookManger::onGetUserInfo( const sdkbox::FBGraphUser& userInfo )
{
    
}
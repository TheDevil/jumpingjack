//
//  FacebookManager.hpp
//  JumpingJack
//
//  Created by Nidhi on 1/25/17.
//
//

#ifndef FacebookManager_hpp
#define FacebookManager_hpp

#include "PluginFacebook/PluginFacebook.h"
#include <stdio.h>


class FacebookManger : public sdkbox::FacebookListener
{
private:
    void onLogin(bool isLogin, const std::string& msg);
    void onSharedSuccess(const std::string& message);
    void onSharedFailed(const std::string& message);
    void onSharedCancel();
    void onAPI(const std::string& key, const std::string& jsonData);
    void onPermission(bool isLogin, const std::string& msg);
    
    void onFetchFriends(bool ok, const std::string& msg);
    void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends );
    void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
    void onInviteFriendsResult( bool result, const std::string& msg );
    
    void onGetUserInfo( const sdkbox::FBGraphUser& userInfo );
    public :
    FacebookManger();
    ~FacebookManger();
};

#endif /* FacebookManager_hpp */
